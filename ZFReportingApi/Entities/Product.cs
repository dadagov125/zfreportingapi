﻿namespace ZFReportingApi.Entities
{
    public class Product:Identity
    {
        
        public string Name { get; set; }
        
        public User User { get; set; }
    }
}