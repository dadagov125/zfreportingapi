﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZFReportingApi.DataBase;
using ZFReportingApi.Entities;

namespace ZFReportingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : Controller
    {
        private readonly ApplicationContext _context;

        public SaleController(ApplicationContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<IActionResult> Get(string productName, string userId, int? skip, int? take, DateTime? from,
            DateTime? to)
        {
            var query = _context.Sales.AsQueryable().Include(s=>s.User).Include(s=>s.Product);
            if (!string.IsNullOrWhiteSpace(productName))
            {
                query.Where(s => s.Product.Name.Contains(productName));
            }

            if (string.IsNullOrWhiteSpace(userId))
            {
                query.Where(p => p.Product.User.Id == userId);
            }

            if (from != null)
            {
                query.Where(s => s.Date >= from);
            }

            if (to != null)
            {
                query.Where(s => s.Date <= to);
            }

            if (skip != null)
            {
                query.Skip(skip.Value);
            }

            if (take != null)
            {
                query.Take(take.Value);
            }

            var sales = await query.ToListAsync();

            return Json(sales);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var sale = await _context.Sales.Include(s=>s.User).FirstOrDefaultAsync(s => s.Id == id);
            if (sale == null)
            {
                return NotFound();
            }

            return Json(sale);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Sale sale)
        {
            if (sale == null)
            {
                return BadRequest();
            }
            

            if (sale.Product?.Id == null && !string.IsNullOrWhiteSpace(sale.Product.Name))
            {
              var product= await  _context.Products.FirstOrDefaultAsync(p => p.Name == sale.Product.Name);
                if (product == null)
                {
                    product = sale.Product;
                    _context.Products.Add(product);
                }
                else
                {
                    sale.Product = product;
                }

            }

            _context.Sales.Add(sale);
            await _context.SaveChangesAsync();

            return Ok(sale);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Sale sale)
        {
            if (sale == null || string.IsNullOrWhiteSpace(sale.Id))
            {
                return BadRequest();
            }

            var existingSale = await _context.Sales.FirstOrDefaultAsync(s => s.Id == sale.Id);

            if (existingSale == null)
            {
                return NotFound();
            }

            existingSale.Count = sale.Count;
            existingSale.Price = sale.Price;
            _context.Sales.Update(existingSale);

            await _context.SaveChangesAsync();

            return Ok(sale);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var sale = await _context.Sales.FirstOrDefaultAsync(s => s.Id == id);
            if (sale == null)
            {
                return NotFound();
            }

            _context.Sales.Remove(sale);

            await _context.SaveChangesAsync();

            return Ok(new {Id=id});
        }
    }
}