﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZFReportingApi.DataBase;
using ZFReportingApi.Entities;

namespace ZFReportingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CostController : Controller
    {
        private readonly ApplicationContext _context;

        public CostController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get( string userId, CostType? type, int? skip, int? take, DateTime? from, DateTime? to)
        {
            var query = _context.Costs.AsQueryable().Include(p=>p.User);
            if (!string.IsNullOrWhiteSpace(userId))
            {
                query.Where(c => c.User.Id==userId);
            }

            if (from != null)
            {
                query.Where(c => c.Date >= from);
            }

            if (to != null)
            {
                query.Where(c => c.Date <= to);
            }
            

            if (type != null)
            {
                query.Where(c => c.CostType == type);
            }

            if (skip != null)
            {
                query.Skip(skip.Value);
            }

            if (take != null)
            {
                query.Take(take.Value);
            }

            var costs = await query.ToListAsync();

            return Json(costs);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var cost = await _context.Costs.Include(p=>p.User).FirstOrDefaultAsync(s => s.Id == id);
            if (cost == null)
            {
                return NotFound();
            }

            return Json(cost);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Cost cost)
        {
            if (cost == null)
            {
                return BadRequest();
            }
            cost.Date=new DateTime(cost.Date.Year,cost.Date.Month,1,12,0,0);
           
            _context.Costs.Add(cost);
            await _context.SaveChangesAsync();

            return Ok(cost);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Cost cost)
        {
            if (cost == null || string.IsNullOrWhiteSpace(cost.Id))
            {
                return BadRequest();
            }
            cost.Date=new DateTime(cost.Date.Year,cost.Date.Month,1,12,0,0);
            var existingCost = await _context.Costs.FirstOrDefaultAsync(c => c.Id == cost.Id);

            if (existingCost == null)
            {
                return NotFound();
            }

            existingCost.AccountingCost = cost.AccountingCost;
            existingCost.CommunalCost = cost.CommunalCost;
            existingCost.CostPrice = cost.CostPrice;
            existingCost.CostType = cost.CostType;
            existingCost.Date = cost.Date;
            existingCost.InsuranceCost = cost.InsuranceCost;
            existingCost.LaborCost = cost.LaborCost;
            existingCost.MarketingCost = cost.MarketingCost;
            existingCost.MinorCost = cost.MinorCost;
            existingCost.RentalCost = cost.RentalCost;
            existingCost.TransportCost = cost.TransportCost;

            _context.Costs.Update(existingCost);

            await _context.SaveChangesAsync();

            return Ok(cost);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var cost = await _context.Costs.FirstOrDefaultAsync(c => c.Id == id);
            if (cost == null)
            {
                return NotFound();
            }

            _context.Costs.Remove(cost);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}