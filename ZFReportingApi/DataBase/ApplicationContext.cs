﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
 using Microsoft.EntityFrameworkCore;
 using ZFReportingApi.Entities;
 
 namespace ZFReportingApi.DataBase
 {
     public class ApplicationContext:IdentityDbContext<User>
     {
         public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
         {
             Database.EnsureCreated();
          
         }
         
         public virtual DbSet<Product> Products { get; set; }
         
         public virtual DbSet<Cost> Costs { get; set; }
         
         public virtual DbSet<Sale> Sales { get; set; }
         
         public virtual DbSet<ConsultClient> ConsultClients { get; set; }
         
     }
 }