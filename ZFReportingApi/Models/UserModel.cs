﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using ZFReportingApi.Entities;

namespace ZFReportingApi.Models
{
    public class UserModel
    {
        public UserModel()
        {
        }

        public UserModel(User user, string role = null)
            : this(user.Id, user.CompanyName, user.FirstName, user.LastName, user.Patronymic, user.UserName, user.Email,
                user.PhoneNumber, role)
        {
        }

        public UserModel(string id, string companyName, string firstName, string lastName, string patronymic,
            string userName, string email, string phoneNumber, string role)
        {
            Id = id;
            CompanyName = companyName;
            FirstName = firstName;
            LastName = lastName;
            Patronymic = patronymic;
            UserName = userName;
            Email = email;
            PhoneNumber = phoneNumber;
            Role = role;
        }

        public string Id { get; set; }

        public string CompanyName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string Role { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}