﻿using System;

namespace ZFReportingApi.Entities
{
    public class Sale:Identity
    {
        public DateTime Date { get; set; }
        
        public decimal Price { get; set; }
        
        public User User { get; set; }
        
        public Product Product { get; set; }
        
        public int Count { get; set; }
        

        
        public SaleType SaleType { get; set; }
    }

    public enum SaleType:byte
    {
        Plan=0,
        Actual=1
    }
}