﻿using System;

namespace ZFReportingApi.Entities
{
    public class Cost:Identity
    {
        
        public CostType CostType { get; set; }
        
        //себестоимость
        public decimal CostPrice { get; set; }
        
        //комунальный расход
        public decimal CommunalCost { get; set; }
        //трансп расход
        public decimal TransportCost { get; set; }
        //расход на аренду
        public decimal RentalCost { get; set; }
        //оплата труда
        public decimal LaborCost { get; set; }
        //мелкие затраты
        public decimal MinorCost { get; set; }
        //маркетинг и продв.
        public decimal MarketingCost { get; set; }
        //аудит и бухучет
        public  decimal AccountingCost { get; set; }
        //страховкаdat
        public decimal InsuranceCost { get; set; }
        
        public User User { get; set; }
        
        public DateTime Date { get; set; }
        
     
        
    }

    public enum CostType:byte
    {
        PLAN=0,
        ACTUAL=1
    }
    
    
    
}