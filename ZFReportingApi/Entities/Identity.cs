﻿using System.ComponentModel.DataAnnotations;

namespace ZFReportingApi.Entities
{
    public abstract class Identity
    {
        [Key] public string Id { get; set; }
    }
}