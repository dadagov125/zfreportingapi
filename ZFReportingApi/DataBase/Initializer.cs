﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ZFReportingApi.Entities;

namespace ZFReportingApi.DataBase
{
    public static class Initializer
    {
        public static async Task InitAsync(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var adminEmail = "admin@mail.ru";
                var consultEmail = "consult@mail.ru";
                var clientEmail = "client@mail.ru";
                
                var admin = "Admin";
                var consult = "Consult";
                var client = "Client";

                var services = scope.ServiceProvider;

                var userManager = services.GetRequiredService<UserManager<User>>();

                var rolesManager = services.GetRequiredService<RoleManager<IdentityRole>>();

                var context = services.GetRequiredService<ApplicationContext>();

          

                User adminUser = await userManager.FindByEmailAsync(adminEmail);

                if (adminUser != null)
                {
                    return;
                }

                adminUser = new User()
                {
                    Email = adminEmail,
                    UserName = adminEmail,
                    FirstName = "Адам",
                    LastName = "Сайдаев",
                    Patronymic = "Алиевич",
                    CompanyName = "Zayed Fund",
                    PhoneNumber = "+79280048881"
                };
                
                
                var consultUser=new User()
                {
                    Email = consultEmail,
                    UserName = consultEmail,
                    FirstName = "Билал",
                    LastName = "Ахидов",
                    Patronymic = "Увайсович",
                    CompanyName = "Zayed Fund",
                    PhoneNumber = "+79280048882"
                };
                
                var clientUser=new User()
                {
                    Email = clientEmail,
                    UserName = clientEmail,
                    FirstName = "Магомед",
                    LastName = "Дадагов",
                    Patronymic = "Русланович",
                    CompanyName = "ИП Дадагов",
                    PhoneNumber = "+79280048883"
                };
                
        

                await userManager.CreateAsync(adminUser, "1234567");
                await userManager.CreateAsync(consultUser, "1234567");
                await userManager.CreateAsync(clientUser, "1234567");

               

                await rolesManager.CreateAsync(new IdentityRole(admin));
                await rolesManager.CreateAsync(new IdentityRole(consult));
                await rolesManager.CreateAsync(new IdentityRole(client));

                await userManager.AddToRoleAsync(adminUser, admin);
                await userManager.AddToRoleAsync(consultUser, consult);
                await userManager.AddToRoleAsync(clientUser, client);
                
                
                var consultclient=new ConsultClient()
                {
                    Consult = consultUser,
                    Client = clientUser
                };

                context.ConsultClients.Add(consultclient);

                var product = new Product()
                {
                    Name = "Ворота",
                    User = clientUser
                };
                var product2 = new Product()
                {
                    Name = "Перила",
                    User = clientUser
                };

                context.Products.AddRange(product, product2);

                var cost1 = new Cost()
                {
                    CostType = CostType.PLAN,
                    CostPrice = 1000,
                    CommunalCost = 200,
                    TransportCost = 50,
                    RentalCost = 1313,
                    LaborCost = 4521,
                    MinorCost = 0,
                    MarketingCost = 3141,
                    AccountingCost = 1342,
                    InsuranceCost = 12,
                    User = adminUser,
                    Date = new DateTime(2018, 12, 1, 0, 0, 0, 0)
                };

                var cost2 = new Cost()
                {
                    CostType = CostType.ACTUAL,
                    CostPrice = 1000,
                    CommunalCost = 200,
                    TransportCost = 50,
                    RentalCost = 1313,
                    LaborCost = 4521,
                    MinorCost = 0,
                    MarketingCost = 3141,
                    AccountingCost = 1342,
                    InsuranceCost = 12,
                    User = clientUser,
                    Date = new DateTime(2018, 11, 1, 0, 0, 0, 0)
                };
                context.Costs.AddRange(cost1, cost2);

                var sale=new Sale()
                {
                    Date = new DateTime(2018, 11, 1, 0, 0, 0, 0),
                    Product = product,
                    Count = 100,
                    Price = 200,
                    User = clientUser,
                    SaleType = SaleType.Actual
                    
                };
                var sale2=new Sale()
                {
                    Date = new DateTime(2018, 12, 2, 0, 0, 0, 0),
                    Product = product,
                    Count = 100,
                    Price = 200,
                    User = adminUser,
                    SaleType = SaleType.Plan
                    
                };

                context.Sales.AddRange(sale, sale2);

              await  context.SaveChangesAsync();
            }
        }
    }
}