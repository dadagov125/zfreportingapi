﻿using System.Collections.Generic;

namespace ZFReportingApi.Entities
{
    public class ConsultClient:Identity
    {
        public User Consult { get; set; }
        
        public User Client { get; set; }
        
    }
}