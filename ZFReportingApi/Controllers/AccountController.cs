﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ZFReportingApi.Entities;
using ZFReportingApi.Models;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ZFReportingApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;

        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

//        [HttpPost]
//        public async Task<IActionResult> Register(RegisterModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                User user = new User {Email = model.Email, UserName = model.Email};
//
//                var result = await _userManager.CreateAsync(user, model.Password);
//                if (result.Succeeded)
//                {
//                    await _signInManager.SignInAsync(user, false);
//                    return Ok();
//                }
//
//                foreach (var error in result.Errors)
//                {
//                    ModelState.AddModelError(error.Code, error.Description);
//                }
//            }
//
//            return BadRequest(ModelState);
//        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            if (ModelState.IsValid)
            {
                SignInResult result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    
                    var user = await _userManager.FindByEmailAsync(model.Email);


                    if (user == null)
                    {
                        return NotFound();
                    }

                    var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

                    return Ok(new UserModel(user, role));
                }

                if (result.IsLockedOut)
                {
                    ModelState.AddModelError("IsLockedOut", "true");
                }
                else if (result.IsNotAllowed)
                {
                    ModelState.AddModelError("IsNotAllowed", "true");
                }
                else if (result.RequiresTwoFactor)
                {
                    ModelState.AddModelError("RequiresTwoFactor", "true");
                }
                else
                {
                    ModelState.AddModelError("LoginOrPassword", "failed");
                }
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Me()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }

            var user = await _userManager.FindByEmailAsync(User.Identity.Name);


            if (user == null)
            {
                return NotFound();
            }

            var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

            return Ok(new UserModel(user, role));
        }
    }
}