﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZFReportingApi.DataBase;
using ZFReportingApi.Entities;
using ZFReportingApi.Models;

namespace ZFReportingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;

        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly ApplicationContext _context;

        public UserController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager,
            ApplicationContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string companyName, string role, int? skip, int? take)
        {
            var query = _userManager.Users.AsQueryable();

            if (!string.IsNullOrEmpty(companyName))
            {
                query.Where(u => u.CompanyName == companyName);
            }

            if (skip != null)
            {
                query.Skip(skip.Value);
            }

            if (take != null)
            {
                query.Take(take.Value);
            }


            var users = await query.ToListAsync();

            List<ConsultClient> consultClients=new List<ConsultClient>();

            if (!string.IsNullOrWhiteSpace(role))
            {
                var roleUsers = await _userManager.GetUsersInRoleAsync(role);
                users = users.Where(u => roleUsers.Contains(u)).ToList();

                if (role == "Consult")
                {

                   consultClients=  _context.ConsultClients.Where(cu => users.Contains(cu.Consult)).ToList();
                }
                
                
            }


            return Json(users.Select(u => new  {User = u, Role = role, Clients=consultClients.Select(cu=>cu.Client).ToList() }));
        }

        [HttpGet("id")]
        public async Task<IActionResult> Get(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Json(user);
        }

        [HttpPost]
        public async Task<IActionResult> Post(EditUserModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    Email = model.Email,
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Patronymic = model.Patronymic,
                    CompanyName = model.CompanyName,
                    PhoneNumber = model.Phone
                };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return Json(user);
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            return BadRequest(ModelState);
        }


        [HttpPut]
        public async Task<IActionResult> Put(EditUserModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Patronymic = model.Patronymic;
                    user.CompanyName = model.CompanyName;
                    user.PhoneNumber = model.Phone;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return Json(user);
                    }

                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }

            return BadRequest(ModelBinderFactory);
        }

        [HttpDelete("id")]
        public async Task<IActionResult> Delete(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            IdentityResult result = await _userManager.DeleteAsync(user);

            if (result.Succeeded)
            {
                return Ok();
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }

            return BadRequest(ModelState);
        }
    }
}