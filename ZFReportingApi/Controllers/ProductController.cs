﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZFReportingApi.DataBase;
using ZFReportingApi.Entities;

namespace ZFReportingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly ApplicationContext _context;

        public ProductController(ApplicationContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<IActionResult> Get(string name, string userId, int? skip, int? take)
        {
            var query = _context.Products.AsQueryable().Include(p=>p.User);
            if (!string.IsNullOrWhiteSpace(name))
            {
                query.Where(p => p.Name.Contains(name));
            }

            if (!string.IsNullOrWhiteSpace(userId))
            {
                query.Where(p => p.User.Id == userId);
            }

            if (skip != null)
            {
                query.Skip(skip.Value);
            }

            if (take != null)
            {
                query.Take(take.Value);
            }

            var products = await query.ToListAsync();

            return Json(products);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var product = await _context.Products.Include(p=>p.User).FirstOrDefaultAsync(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return Json(product);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Product product)
        {
            if (product == null)
            {
                return BadRequest();
            }

            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return Ok(product);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Product product)
        {
            if (product == null || string.IsNullOrWhiteSpace(product.Id))
            {
                return BadRequest();
            }

            var existingProduct = await _context.Products.FirstOrDefaultAsync(p => p.Id == product.Id);

            if (existingProduct == null)
            {
                return NotFound();
            }

            existingProduct.Name = product.Name;

            _context.Products.Update(existingProduct);

           await _context.SaveChangesAsync();

            return Ok(product);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}